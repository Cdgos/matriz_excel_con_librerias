/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Vista.Vista;
import java.io.File;
import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetras
{
   public String posiciones="";
    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras()
    {
        
    }

    
    
   
    public SopaDeLetras(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }
        
     //Crear la matriz con las correspondientes filas:
     
     String palabras2[]=palabras.split(",");
     this.sopas=new char[palabras2.length][];
     int i=0;
     for(String palabraX:palabras2)
     {
         //Creando las columnas de la fila i
         this.sopas[i]=new char[palabraX.length()];
         pasar(palabraX,this.sopas[i]);
         i++;
        
     }
     
     
    }
    
    private void pasar (String palabra, char fila[])
    {
    
        for(int j=0;j<palabra.length();j++)
        {
            fila[j]=palabra.charAt(j);
        }
    }
    
    
    public String toString()
    {
    String msg="";
    for(int i=0;i<this.sopas.length;i++)
    {
        for (int j=0;j<this.sopas[i].length;j++)
        {
            msg+=this.sopas[i][j]+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    public String toString2()
    {
    String msg="";
    for(char filas[]:this.sopas)
    {
        for (char dato :filas)
        {
            msg+=dato+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    
    public boolean esCuadrada()
    { if(sopas==null){return false;}
       int i=0;
       int j=0;
       while(i<sopas.length && sopas.length>1 ){
       if(sopas.length==sopas[i].length){
         j++;
        }
        i++;
    }
      if(j==sopas.length){
        return true;
       }
    
     return false;
    
}

    
     public boolean esDispersa()
    {  
       if(sopas==null){return false;}
        for(int i=0;i<this.sopas.length;i++)
    {
        for (int j=0;j<this.sopas[i].length;j++)
        {
            if(sopas[i].length != sopas[0].length){
                return true;
            }
            }
        
        
   }

    return false;
}

    public boolean esRectangular()
    {
       if(sopas==null){return false;} 
        for(int i=0;i<this.sopas.length;i++)
    {
       if(esCuadrada()== false && esDispersa()==false && (sopas[i].length >1 || sopas.length>1 )){
        
        return true;
        }
    }
        return false;
    }
    /*
        retorna cuantas veces esta la palabra en la matriz
       */
   
        public int getContar(String palabra)
    {
        int contar = 0;
     contar=buscarHorizontalDerecha(palabra)+buscarHorizontalIzquierda(palabra)+buscarVerticalAbajo(palabra)+
             buscarVerticalArriba(palabra);
        
    
        
        return contar;
    }
    /*
        debe ser cuadrada sopas
       */
    public char[] getDiagonalPrincipal() throws Exception
    { 
        char[]diagoPrincipal= new char[sopas.length];
       if(esCuadrada()==true){
        for(int i=0;i<sopas.length;i++){
    diagoPrincipal[i]= sopas[i][i];  
}
}
else{ 
 throw new Exception("Error no se puede crear la Diagonal Principal");
 
}
 
return diagoPrincipal;
}
    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
    public int buscarHorizontalDerecha(String palabra){
        int cantidad = 0;
        int k=0;
        int x=0;
        int y=0;

    
        for(int i=0; i<sopas.length; i++){
            for(int j=0; j<sopas[i].length; j++){
            
            if(sopas[i][j]==palabra.charAt(k)){
            
            if(k==0){
            x= i;
            y= j;
            }
            k++; 
               
               
            }else{
                  k=0;
                 
                  
                  if(sopas[i][j]==palabra.charAt(k))
                   {
                        k++;
                   }
            }
            
            if(k==palabra.length()){
             cantidad++;
             k=0;
             posiciones+="\n"+" -Se encontro horizontalmente en la fila ["+x+"], columna ["+y+"] de izquierda a derecha";
             
            }else{
            if(k>0 && j==sopas[i].length-1){
            k=0;
            
            }
               
            }
                
            }
        }
        
    
        return cantidad;
    }
 
    
    public int buscarHorizontalIzquierda(String palabra){
        int cantidad = 0;
        int k=0;
        int x=0; int y=0;
    
        for(int i=sopas.length-1; i>=0; i--){
            for(int j=sopas[i].length-1; j>=0; j--){
            
            if(sopas[i][j]==palabra.charAt(k)){
               if(k==0){
            x= i;
            y= j;
            }
                k++;
            }else{
                  k=0;
                  if(sopas[i][j]==palabra.charAt(k))
                   {
                        k++;
                   }
            }
            
            if(k==palabra.length()){
             cantidad++;
             k=0;
             posiciones+="\n"+" -Se encontro horizontalmente en la fila ["+x+"], columna ["+y+"] de derecha a izquierda";
            }else{
            if(k>0 && j-1==sopas[i].length-1){
             k=0;  
            }
                
            }
        }
    }
        return cantidad;
    }
    
    public int buscarVerticalAbajo(String palabra){
    int cantidad=0;
    int k=0;
    int x=0; int y=0;
    
    for(int i=0; i<sopas[0].length; i++){
        for(int j=0; j<sopas.length; j++){
           
            if(sopas[j][i]==palabra.charAt(k)){
               if(k==0){
               x=j;
               y=i;
            }
            k++;
            }else{
            k=0;
            if(sopas[j][i]==palabra.charAt(k)){
            k++;
            }
            }
            
            
            if(k==palabra.length()){
            cantidad++;
            k=0;
            posiciones+="\n"+" -Se encontro verticalmente en la fila ["+x+"], columna ["+y+"] de arriba hacia abajo";
            }else{
            if(k>0 && j==sopas.length-1){
                k=0;
            }
            }
        }        
        }
  
    return cantidad;
    
    }
    
    public int buscarVerticalArriba(String palabra){
    int cantidad=0;
    int k=0;
    int x=0; int y=0;
    
   for(int i=sopas[0].length-1; i>=0; i--){
        for(int j=sopas.length-1; j>=0; j--){
           
            if(sopas[j][i]==palabra.charAt(k)){
             if(k==0){
                x= j;
                y= i;
            }
               k++;
            }else{
                k=0;
                if(sopas[j][i]==palabra.charAt(k)){
                k++;
            }
            }
            
            
            if(k==palabra.length()){
            cantidad++;
            k=0;
            posiciones+="\n"+" -Se encontro verticalmente en la fila ["+x+"], columna ["+y+"] de abajo hacia arriba";
            }
            
            
        }        
        }
    
    return cantidad;
    
    }
    public String diagonal(String palabra){
    for(int i=0; i<sopas[i].length; i++){
        for(int j=0; j<sopas.length; j++){
           String palabraEncontrada="";
            if(sopas[i][j]==palabra.charAt(0)){
                int[]pos={i,j};
                // diagonal subiendo hacia la derecha.
  	  palabraEncontrada = buscar(pos, palabra.length(), -1, 1);
  	  
  	  if(palabraEncontrada.equals(palabra))
  		  return "se encontró "+ palabra +" desde la posición [" + pos[0] + "," + pos[1] + "] sobre la diagonal subiendo derecha";
         //diagonal bajando hacia la izquierda.
  	  palabraEncontrada = buscar(pos, palabra.length(), 1, -1);
  	  if(palabraEncontrada.equals(palabra)) 
  		  return "se encontró "+ palabra +"  desde la posición [" + pos[0] + "," + pos[1] + "] sobre la diagonal bajando izquierda";
  		  
  	  //diagonal subiendo hacia la izquierda.
  	  palabraEncontrada = buscar(pos, palabra.length(), -1, -1);
  	  if(palabraEncontrada.equals(palabra))
  		  return "se encontró"+ palabra +" desde la posición [" + pos[0] + "," + pos[1] + "] sobre la diagonal subiendo izquierda";

  	  //diagonal bajando hacia la derecha.
  	  palabraEncontrada = buscar(pos, palabra.length(), 1, 1);
  	  if(palabraEncontrada.equals(palabra))
  		  return "se encontró "+ palabra +" desde la posición [" + pos[0] + "," + pos[1] + "] sobre la diagonal bajando derecha";

            }
        }
    
    
    
    }
    return "no se encontró" +palabra;
    }
    
    public String buscar(int[] posInicial, int tamanioPalabra, int moverEnFila, int moverEnColumna) {
  	String palabra = "";
  	int recorrido = 0;
         int fila = posInicial[0], columna = posInicial[1];

  	while((recorrido < tamanioPalabra) && (fila < sopas.length && columna < sopas.length) && (fila > -1 && columna > -1)) {

  	  palabra += sopas[fila][columna];
  	  fila = fila + moverEnFila;
  	  columna = columna + moverEnColumna;
  	  recorrido++;
  	}

  	return palabra;
}
    
   
     public char[][] leerExcel() throws Exception {
         File excel= new File(Vista.johan);
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(excel));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum()+1;
       /* System.out.println("Filas:"+canFilas);
         String valor = null;
*/
       char[][]s= new char [canFilas][];
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            int cantCol=filas.getLastCellNum();
           s[i]= new char[cantCol];
           
        for(int j=0;j<cantCol;j++)    
        {
            //Obtiene la celda y su valor respectivo
            //double r=filas.getCell(i).getNumericCellValue();
           //valor=filas.getCell(j).getStringCellValue();
            //System.out.print(valor+"\t");
            s[i][j]= filas.getCell(j).getStringCellValue().charAt(0);
        }
     //System.out.println();
       }
   return s;
    }
}

